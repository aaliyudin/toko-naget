<?php

use Illuminate\Database\Seeder;

use App\Model\Permission;

class PermissionRoTableSeeder extends Seeder
{
    public function run()
    {
        /**
         *  Laratrust Permission Guide
         *  1. Format is [Permission]-[Module]
         *  2. Submodule separated by underscore, eg: create-module_submodule
         *  3. Standard permission is CRUD (Create, Read, Update, Delete)
         *  4. Custom Permission including
         *     - Menu -> Able to see the link on the sidebar
         *     - Reveal -> Reveal all the informations (no asterisk)
         */

        $permission = [
          // request Order
          [   'name' => 'create-requestorder',
              'display_name' => '[request Order] Create request Order',
              'description' => 'Allowed To Create request Order Data' ],
          [   'name' => 'read-requestorder',
              'display_name' => '[request Order] Read request Order',
              'description' => 'Allowed To Read request Order Data' ],
          [   'name' => 'update-requestorder',
              'display_name' => '[request Order] Update request Order',
              'description' => 'Allowed To Update request Order Data' ],
          [   'name' => 'menu-requestorder_create',
              'display_name' => '[request Order] Show Menu request Order Create',
              'description' => 'Allowed To See Menu request Order Create In Sidebar' ],
          [   'name' => 'menu-requestorder_update',
              'display_name' => '[request Order] Show Menu request Order Update',
              'description' => 'Allowed To See Menu request Order Update In Sidebar' ],
          // RO Payment
          [   'name' => 'read-ropayment',
              'display_name' => '[RO Payment] Read request Order Payment',
              'description' => 'Allowed To Read request Order Payment Data' ],
          [   'name' => 'menu-ropayment',
              'display_name' => '[RO Payment] Show Menu request Order Payment',
              'description' => 'Allowed To See Menu request Order Payment In Sidebar' ],
          // RO Payment Cash
          [   'name' => 'create-ropaymentcash',
              'display_name' => '[RO Payment Cash] Create Cash Payment For request Order',
              'description' => 'Allowed To Create Cash Payment For request Order Data' ],
          // RO Payment Transfer
          [   'name' => 'create-ropaymenttransfer',
              'display_name' => '[RO Payment Transfer] Create Transfer Payment For request Order',
              'description' => 'Allowed To Create Transfer Payment For request Order Data' ],
          // RO Payment Giro
          [   'name' => 'create-ropaymentgiro',
              'display_name' => '[RO Payment Giro] Create Giro Payment For request Order',
              'description' => 'Allowed To Create Giro Payment For request Order Data' ],
          // RO Copy
          [   'name' => 'create-rocopy',
              'display_name' => '[RO Copy] Create request Order Copy',
              'description' => 'Allowed To Create request Order Copy Data' ],
          [   'name' => 'read-rocopy',
              'display_name' => '[RO Copy] Read request Order Copy',
              'description' => 'Allowed To Read request Order Copy Data' ],
          [   'name' => 'update-rocopy',
              'display_name' => '[RO Copy] Update request Order Copy',
              'description' => 'Allowed To Update request Order Copy Data' ],
          [   'name' => 'delete-rocopy',
              'display_name' => '[RO Copy] Delete request Order Copy',
              'description' => 'Allowed To Delete request Order Copy Data' ],
          [   'name' => 'menu-rocopy',
              'display_name' => '[RO Copy] Show Menu request Order Copy',
              'description' => 'Allowed To See Menu request Order Copy In Sidebar' ],
        ];

        foreach ($permission as $key => $value) {
            if (count(Permission::whereName($value['name'])->get()) == 0) {
                Permission::create($value);
            }
        }
    }
}
